﻿using UnityEngine;
using System.Collections;

public class drag : MonoBehaviour {
	private Vector3 screenPoint;
	private Vector3 offset;

	private Plane moveAxis;
	private float intersect;

	private Vector3 mouseOriginalPos;

	private int gridSize = 1;

	void OnMouseDown() {
//		Debug.Log ("On mouse down called");
		moveAxis = new Plane(Vector3.up, gameObject.transform.position);
		mouseOriginalPos = Input.mousePosition;

	}

	void OnMouseDrag() {
//		Debug.Log ("On mouse drag called");

		Vector3 currentMousePosition = Input.mousePosition;
		if (Mathf.Abs(mouseOriginalPos.magnitude - currentMousePosition.magnitude) > 3) {
			onMouseActualDrag();
		}
	}

	void onMouseActualDrag() {
//		Debug.Log ("On mouse actual drag called");
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (moveAxis.Raycast(ray, out intersect)) {
			Vector3 movePoint = ray.GetPoint(intersect);
//			Debug.Log("x "+movePoint.x+" y "+movePoint.y+" z "+movePoint.z);
			transform.position = new Vector3(movePoint.x,
				movePoint.y,
				movePoint.z);
		}
	}
}
