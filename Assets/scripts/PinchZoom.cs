﻿using UnityEngine;
using System.Collections;

public class PinchZoom : MonoBehaviour {
	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
	
	
	void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit(); 
			return;
		}
		// If there are two touches on the device...
		if (Input.touchCount == 2) {
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			float dotProduct = Vector2.Dot(touchZero.deltaPosition.normalized, touchOne.deltaPosition.normalized);

			if (dotProduct < 0.5) {
				ZoomUpdate(touchZero, touchOne);
			} else if (dotProduct > 0.5) {
				PanUpdate();
			}
		}
	}

	void PanUpdate() {

	}

	void ZoomUpdate(Touch touchZero, Touch touchOne) {
		// Find the position in the previous frame of each touch.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		
		// Find the difference in the distances between each frame.
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
		
		// If the camera is orthographic...
		if (GetComponent<Camera> ().orthographic) {
			// ... change the orthographic size based on the change in distance between the touches.
			GetComponent<Camera> ().orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
			
			// Make sure the orthographic size never drops below zero.
			GetComponent<Camera> ().orthographicSize = Mathf.Max (GetComponent<Camera> ().orthographicSize, 0.1f);
		} else {
			// Otherwise change the field of view based on the change in distance between the touches.
			GetComponent<Camera> ().fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
			
			// Clamp the field of view to make sure it's between 0 and 180.
			GetComponent<Camera> ().fieldOfView = Mathf.Clamp (GetComponent<Camera> ().fieldOfView, 0.1f, 179.9f);
		}
	}
}