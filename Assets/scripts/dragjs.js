﻿#pragma strict

function Start () {

}

var hit : RaycastHit;
var moveAxis : Plane;
private var object : Transform;
var ray : Ray;
var intersect : float;
var movePoint : Vector3;
    
function Update () {
 ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    if(Input.GetMouseButtonDown(0)){
        if(!object){
            if(Physics.Raycast(ray, hit, 100)){
                object = hit.transform;
                moveAxis = Plane(Vector3.up, object.position);
            }
        }
    } else if(Input.GetMouseButtonUp(0)) {
        object = null;
    }
   
    if(object){
        if(moveAxis.Raycast(ray, intersect)) {
            movePoint = ray.GetPoint(intersect);
//            Debug.Log("x "+movePoint.x+" y "+movePoint.y+" z "+movePoint.z);
            object.position = movePoint;
        }
    }
}