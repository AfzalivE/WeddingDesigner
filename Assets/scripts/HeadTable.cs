﻿using UnityEngine;
using System.Collections;

public class HeadTable : MonoBehaviour {

	public GameObject tableFab;
	public int numLength;
	public GameObject chairFab;
	public int numChairs;
	public GameObject parent;

	public void CreateTable() {
		
		GameObject parentTable = Instantiate(tableFab, new Vector3(0, 2, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
		GameObject table = parentTable.transform.GetChild(0).gameObject as GameObject;

		int offset = 4;
		for (int i = 1; i < numLength; i++) {
			GameObject tableClone = Instantiate(table, new Vector3(offset, 2, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
			tableClone.transform.parent = parentTable.transform;
			offset += 4;
		}

		parentTable.transform.parent = parent.transform;
		BoxCollider collider = parentTable.GetComponent<BoxCollider>();
		// collider center x is in the middle of the parent object
		// so width of the whole thing / 2 - width of one of the cubes / 2
		collider.center = new Vector3(offset / 2 - 4 / 2, collider.center.y, collider.center.z);
		collider.size = new Vector3(offset, collider.size.y, collider.size.z);

		for (int i = 0; i < numChairs; i++) {
			float qx, qy, qz, qw;
			float rotY = 0;
			float posX = 0, posZ = 0;

			switch (i) {
				case 0:
					posX = 4;
					posZ = 4;
					rotY = 180;
					break;
				case 1:
					posX = 8;
					posZ = 4;
					rotY = 180;
					break;
				case 2:
					posX = 0;
					posZ = 4;
					rotY = 180;
					break;
				case 3:
					posX = 12;
					posZ = 4;
					rotY = 180;
					break;
				case 4:
					posX = -4;
					posZ = 0;
					rotY = 90;
					break;
				case 5:
					posX = 16;
					posZ = 0;
					rotY = -90;
					break;
				case 6:
					posX = 0;
					posZ = -4;
					rotY = 0;
					break;
				case 7:
					posX = 4;
					posZ = -4;
					rotY = 0;
					break;
				case 8:
					posX = 8;
					posZ = -4;
					rotY = 0;
					break;
				case 9:
					posX = 12;
					posZ = -4;
					rotY = 0;
					break;
			}

			GuestTable.calculateQuat(0, rotY, 0, out qx, out qy, out qz, out qw);
			GameObject chair = Instantiate(chairFab, new Vector3(posX, 0, posZ), new Quaternion(qx, qy, qz, qw)) as GameObject; // left
			chair.transform.parent = parentTable.transform;
		}
	}
}
