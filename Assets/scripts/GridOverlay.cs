﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridOverlay : MonoBehaviour {

	public GameObject plane;

	public bool showMain = true;
	public bool showSub = false;

	private int gridSizeX;
	private int gridSizeY;
	private int gridSizeZ;

	public float smallStep;
	public float largeStep;

	private float startX;
	private float startY;
	private float startZ;

	private float offsetY = 0;
	private float scrollRate = 0.1f;
	private float lastScroll = 0f;

	private Material lineMaterial;

	public Color mainColor = new Color(0f, 1f, 0f, 1f);
	public Color subColor = new Color(0f, 0.5f, 0f, 1f);

	void Start() {
		startX = plane.transform.localPosition.x - plane.GetComponent<Renderer>().bounds.size.x / 2;
		startY = plane.transform.localPosition.y;
		startZ = plane.transform.localPosition.z - plane.GetComponent<Renderer>().bounds.size.z / 2;

//		Debug.Log("pos: " + startX + "," + startY + "," + startZ);

		gridSizeX = Mathf.RoundToInt(plane.GetComponent<Renderer>().bounds.size.x);
		gridSizeY = 0;
		gridSizeZ = Mathf.RoundToInt(plane.GetComponent<Renderer>().bounds.size.z);
	}

	void Update() {
		if (lastScroll + scrollRate < Time.time) {
			if (Input.GetKey(KeyCode.KeypadPlus)) {
				plane.transform.position = new Vector3(plane.transform.position.x, plane.transform.position.y + smallStep, plane.transform.position.z);
				offsetY += smallStep;
				lastScroll = Time.time;
			}
			if (Input.GetKey(KeyCode.KeypadMinus)) {
				plane.transform.position = new Vector3(plane.transform.position.x, plane.transform.position.y - smallStep, plane.transform.position.z);
				offsetY -= smallStep;
				lastScroll = Time.time;
			}
		}
	}

	void CreateLineMaterial() {
	
		if (!lineMaterial) {
			lineMaterial = new Material(Shader.Find("Custom/GizmoShader"));

//				lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
//				"SubShader { Pass { " +
//				"    Blend SrcAlpha OneMinusSrcAlpha " +
//				"    ZWrite Off Cull Off Fog { Mode Off } " +
//				"    BindChannels {" +
//				"      Bind \"vertex\", vertex Bind \"color\", color }" +
//				"} } }");
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		}
	}

	private IDictionary<Color, Material> materialsByColor = new Dictionary<Color, Material>();

	private Material GetLineMaterial(Color color) {
		Material lineMaterial;
		if (!materialsByColor.TryGetValue(color, out lineMaterial)) {
			lineMaterial = new Material(Shader.Find("Custom/GizmoShader"));
			lineMaterial.SetColor("Main Color", color);

//			lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
//			" Properties { _Color (\"Main Color\", Color) = (" + color.r + "," + color.g + "," + color.b + "," + color.a + ") } " +
//			" SubShader { Pass { " +
//			" Blend SrcAlpha OneMinusSrcAlpha " +
//			" ZWrite Off Cull Off Fog { Mode Off } " +
//			" Color[_Color] " +
//			" BindChannels {" +
//			" Bind \"vertex\", vertex Bind \"color\", color }" +
//			"} } }");
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;

			materialsByColor.Add(color, lineMaterial);
		}
		return lineMaterial;
	}

	void OnPostRender() {        
		CreateLineMaterial();
		// set the current material
		lineMaterial.SetPass(0);

		GL.Begin(GL.LINES);

		if (showSub) {
			GL.Color(subColor);
//			var lineMaterial = GetLineMaterial(subColor);
//			lineMaterial.SetPass(0);

			//Layers
			for (float j = 0; j <= gridSizeY; j += smallStep) {
				//X axis lines
				for (float i = 0; i <= gridSizeZ; i += smallStep) {
					GL.Vertex3(startX, j + offsetY, startZ + i);
					GL.Vertex3(gridSizeX, j + offsetY, startZ + i);
				}

				//Z axis lines
				for (float i = 0; i <= gridSizeX; i += smallStep) {
					GL.Vertex3(startX + i, j + offsetY, startZ);
					GL.Vertex3(startX + i, j + offsetY, gridSizeZ);
				}
			}

			//Y axis lines
			for (float i = 0; i <= gridSizeZ; i += smallStep) {
				for (float k = 0; k <= gridSizeX; k += smallStep) {
					GL.Vertex3(startX + k, startY + offsetY, startZ + i);
					GL.Vertex3(startX + k, gridSizeY + offsetY, startZ + i);
				}
			}
		}

		if (showMain) {
			GL.Color(mainColor);
//			var lineMaterial = GetLineMaterial(mainColor);
//			lineMaterial.SetPass(0);

			//Layers
			for (float j = 0; j <= gridSizeY; j += largeStep) {
				//X axis lines
				for (float i = 0; i <= gridSizeZ; i += largeStep) {
					GL.Vertex3(startX, j + offsetY, startZ + i);
					GL.Vertex3(gridSizeX, j + offsetY, startZ + i);
				}

				//Z axis lines
				for (float i = 0; i <= gridSizeX; i += largeStep) {
					GL.Vertex3(startX + i, j + offsetY, startZ);
					GL.Vertex3(startX + i, j + offsetY, gridSizeZ);
				}
			}

			//Y axis lines
			for (float i = 0; i <= gridSizeZ; i += largeStep) {
				for (float k = 0; k <= gridSizeX; k += largeStep) {
					GL.Vertex3(startX + k, startY + offsetY, startZ + i);
					GL.Vertex3(startX + k, gridSizeY + offsetY, startZ + i);
				}
			}
		}


		GL.End();
	}
}