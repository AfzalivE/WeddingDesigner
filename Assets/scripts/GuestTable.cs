﻿using UnityEngine;
using System.Collections;

public class GuestTable : MonoBehaviour {

	public GameObject tableFab;
	public GameObject chairFab;
	public int numChairs;
	public GameObject parent;

	public void CreateTable() {
		GameObject table = Instantiate(tableFab, new Vector3(0, 1.35f, 0), new Quaternion(1, 0, 0, -1)) as GameObject;

		if (numChairs > 8) {
			numChairs = 8;
			Debug.Log("Can only have 8 chairs max");
		}

		for (int i = 0; i < numChairs; i++) {
			float qx, qy, qz, qw;
			float rotY = 0;
			float posX = 0, posZ = 0;
			switch (i) {
				case 0:
					posX = 0;
					posZ = -4;
					rotY = 0;
					break;
				case 1:
					posX = 0;
					posZ = 4;
					rotY = 180;
					break;
				case 2:
					posX = -4;
					posZ = 0;
					rotY = 90;
					break;
				case 3:
					posX = 4;
					posZ = 0;
					rotY = 270;
					break;
				case 4:
					posX = -3;
					posZ = -3;
					rotY = 45;
					break;
				case 5:
					posX = -3;
					posZ = 3;
					rotY = 135;
					break;
				case 6:
					posX = 3;
					posZ = 3;
					rotY = 225;
					break;
				case 7:
					posX = 3;
					posZ = -3;
					rotY = 315;
					break;
			}
			calculateQuat(0, rotY, 0, out qx, out qy, out qz, out qw);

			GameObject chair = Instantiate(chairFab, new Vector3(posX, 0, posZ), new Quaternion(qx, qy, qz, qw)) as GameObject; // left
			chair.transform.parent = table.transform;
		}
//		GameObject chair = Instantiate(chairFab, new Vector3(0, 0, -4), new Quaternion(0, 0, 0, 0)) as GameObject; // down

		table.transform.parent = parent.transform;
	}

	public static void calculateQuat(float x, float y, float z, out float qx, out float qy, out float qz, out float qw) {
		float h = y * Mathf.PI / 360;
		float a = z * Mathf.PI / 360;
		float b = x * Mathf.PI / 360;
		float c1 = Mathf.Cos(h);
		float c2 = Mathf.Cos(a);
		float c3 = Mathf.Cos(b);
		float s1 = Mathf.Sin(h);
		float s2 = Mathf.Sin(a);
		float s3 = Mathf.Sin(b);
		qw = Mathf.Round((c1 * c2 * c3 - s1 * s2 * s3) * 100000) / 100000;
		qx = Mathf.Round((s1 * s2 * c3 + c1 * c2 * s3) * 100000) / 100000;
		qy = Mathf.Round((s1 * c2 * c3 + c1 * s2 * s3) * 100000) / 100000;
		qz = Mathf.Round((c1 * s2 * c3 - s1 * c2 * s3) * 100000) / 100000;

//		Debug.Log("Quaternion: " + qx + ", " + qy + ", " + qz + ", " + qw);
	}
}

