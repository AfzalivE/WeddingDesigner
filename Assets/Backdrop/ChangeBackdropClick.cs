﻿using UnityEngine;
using System.Collections;

public class ChangeBackdropClick : MonoBehaviour {
	public GameObject backdropObject;
	public Material[] myMats;

	public void TestClick() {
		Material[] mats = backdropObject.GetComponent<Renderer>().materials;
		Material[] sharedMats = backdropObject.GetComponent<Renderer>().sharedMaterials;
		Debug.Log("Original material: " + mats[0]);

		int i = 0;
		for (i = 0; i < myMats.Length; i++) {
			Debug.Log(sharedMats[0].name + " vs " + myMats[i].name);
			Debug.Log(sharedMats[0].name.Contains(myMats[i].name));
			if (sharedMats[0].name.Contains(myMats[i].name)) {
				break;
			}
		}

		if (i >= myMats.Length - 1) {
			i = 0;
		} else {
			i += 1;
		}

		Debug.Log("New material: " + myMats[i]);

		mats[0] = myMats[i];
		backdropObject.GetComponent<Renderer>().materials = mats;
		Debug.Log("Applied material: " + backdropObject.GetComponent<Renderer>().materials[0]);
	}

}
