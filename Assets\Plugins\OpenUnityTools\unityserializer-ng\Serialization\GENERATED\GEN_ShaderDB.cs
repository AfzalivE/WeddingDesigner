using System;
using System.Collections.Generic;

///THIS CODE WAS AUTOMATICALLY GENERATED!
public partial class StoreMaterials {
public StoreMaterials() {

if (ShaderDatabase.Count == 0) {
ShaderDatabase.Add("FX/MirrorReflection", new List<MaterialProperty> {
new MaterialProperty {
name = "_MainTex",
type = MaterialProperty.PropertyType.TexEnv
},
new MaterialProperty {
name = "_ReflectionTex",
type = MaterialProperty.PropertyType.TexEnv
}
});

ShaderDatabase.Add("Transparent/Glass", new List<MaterialProperty> {
new MaterialProperty {
name = "_Color",
type = MaterialProperty.PropertyType.Color
},
new MaterialProperty {
name = "_SpecColor",
type = MaterialProperty.PropertyType.Color
},
new MaterialProperty {
name = "_Shininess",
type = MaterialProperty.PropertyType.Range
},
new MaterialProperty {
name = "_ReflectColor",
type = MaterialProperty.PropertyType.Color
},
new MaterialProperty {
name = "_MainTex",
type = MaterialProperty.PropertyType.TexEnv
},
new MaterialProperty {
name = "_Cube",
type = MaterialProperty.PropertyType.TexEnv
}
});


}
}
}
